const knex = require("knex");
const configuration = require("../../knexfile");

const config = process.env.NODE_ENV == "test";
const connection = knex(config ? configuration.test : configuration.development);

module.exports = connection;
