const request = require("supertest");
const app = require("../../src/app");
const connection = require("../../src/database/connection");

describe("ONG", () => {
	beforeEach(async () => {
		connection.migrate
			.rollback()
			.then(function() {
				connection.migrate.latest();
			})
			.then(function() {
				connection.seed.run();
			})
			.then(() => {
				done();
			});
	});

	afterAll(async () => {
		await connection.destroy();
	});

	it("should be able to create a new ONG", async () => {
		const res = await request(app)
			.post("/api/ongs")
			// .set("Authorization", "762fbc3f")
			.send({
				name: "APAD",
				email: "email@servidor.com",
				whatsapp: "4400001111",
				city: "maringa",
				uf: "pr"
			});

		expect(res.body).toHaveProperty("id");
		expect(res.body.id).toHaveLength(8);
	});
});
